Package dependencies for the **climex** package in **R-3.4.0** on a
**Ubuntu16.04** image 

### 1.1.0

Depends on **r-gis-ubuntu:1.1.0**, which in addition includes the
**Ubuntu** packages *libnetcdf-dev* and *netcdf-bin* and the **R**
package *ncdf4*.

### 1.0.0

Based on the **r-gis-ubuntu:1.0.0** image. 

Contains the following **R** packages (installed at the 27.08.2018):

*"dplyr", "xts", "devtools", "ggplot2", "tidyr", "tibble",
"latex2exp", "lubridate", "bookdown", "pander", "Rcpp",
"RcppArmadillo", "markdown", "dygraphs", "shiny", "htmltools",
"shinytoastr", "RCurl", "RColorBrewer", "deseasonalize",
"Lmoments", "moments", "numDeriv", "alabama", "bookdown",
"grid", "gridExtra", "ds", "microbenchmark", "Cairo",
"bookdown", "OpenStreetMap"*

and the following custom packages.

*climex* from [Github](https://github.com/theGreatWhiteShark/climex)
on commit 2fe201c

*dfoptim* from [Github](https://github.com/theGreatWhiteShark/dfoptim)
on commit c108996
