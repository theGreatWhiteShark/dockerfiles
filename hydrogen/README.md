**Debian Stretch** with a specific version of the **Hydrogen** drum
machine and a **JACKd1** and **x11vnc** server to start it up.

### edc8584.0

It is based on the [thegreatwhiteshark/hydrogen-dependencies:0.1](https://hub.docker.com/r/thegreatwhiteshark/hydrogen-dependencies/)
images and contains **Hydrogen** compiled for **commit edc8584**. 

The password of the **x11vnc** server is set to *hydrogen*.

The following line was included in the *.bashrc* and will start up the
JACKdv1 server whenever you run the image inside of a container.
_jackd -R -d alsa -d hw:0 &_
Due to this command you have to press return once in order to have a
prompt in the a remote session.

### Starting up the VNC server

To start up the **VNC server** you first have to run the image in a
container using the following command 

``` bash
sudo docker run -p 5900 --rm --privileged=true --device=/dev/snd:/dev/snd thegreatwhiteshark/hydrogen:edc8584.0 x11vnc -forever -usepw -create
```

Then, in another terminal, run `sudo docker ps` to find the
port of the container the *hydrogen-dependencies* image is running in
(e.g. 0.0.0.0:32770). Connect to this IP via a VNC client using the
password "hydrogen". Finally, press return in the xterm you are
landing in order to obtain a prompt and lunch hydrogen using the
following command

``` bash
./root/hydrogen/build/src/gui/hydrogen --verbose=DEBUG
```

### Building your own Docker image for Hydrogen

If you intend to build your own customized Docker image for a specific
Hydrogen release, I suggest you use the this image as a template and
replace the command `git checkout edc8584` with the commit or tag you
want your version to be of.

Use the following links for instructions about how to build a Docker
image
https://docs.docker.com/get-started/part2/#log-in-with-your-docker-id
