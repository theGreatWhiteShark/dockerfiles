An **Ubuntu16.04** system with **Pandoc**, a compiled
version of **R-3.4.0**, and up-to-date GIS packages.

### 1.1.0

In addition, the **Ubuntu** packages *libnetcdf-dev* and *netcdf-bin* and
the **R** package *ncdf4* have been added.

### 1.0.0

This image depends on **r-ubuntu:1.1.2**, which was installed from
the Ubuntu repositories. 

The **R-3.4.0** version was compiled from source and a number of
system packages have been installed to do so.

*gfortran fort77 libreadline6 libreadline6-dev xorg-dev bzip2
libbz2-dev liblzma-dev libtiff5-dev libpcre3-dev libxml2-dev
libssl-dev libuv1-dev unzip libcairo2-dev libv8-3.14-dev
libprotobuf-dev libgdal-dev gdal-bin libproj-dev libudunits2-dev
libv8-3.14-dev libprotobuf-dev libgdal-dev gdal-bin libproj-dev
libudunits2-dev default-jdk*
